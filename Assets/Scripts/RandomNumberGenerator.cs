using System;
using System.Collections;
using System.Collections.Generic;

public class RandomNumberGenerator {

  private int seed;
  private System.Random rngRange;

  /// <summary>
  /// Class Constructor
  /// </summary>
  public RandomNumberGenerator(int seed) {
    SetSeed(seed);
  }

  /// <summary>
  /// Returns a random integer between and including the specified values.
  /// </summary>
  public int RandomRangeI(int minimum, int maximum) {
    return rngRange.Next(minimum, maximum);
  }

  /// <summary>
  /// Returns a random float between and including the specified values.
  /// </summary>
  public float RandomRangeF(float minimum, float maximum) {
    return (float) rngRange.NextDouble() * (maximum - minimum) + minimum;
  }

  /// <summary>
  /// Returns a random integer.
  /// </summary>
  public int RandomNext(){
    return rngRange.Next();
  }

  /// <summary>
  /// Resets the generator with the localy stored seed.
  /// </summary>
  public void ResetRng(){
    rngRange = new System.Random(this.seed);
  }

  /// <summary>
  /// Resets the generator with the specified seed.
  /// </summary>
  public void UseSeed(int seed){
    rngRange = new System.Random(seed);
  }

  /// <summary>
  /// Resets the generator with the specified seed and sorts it localy.
  /// <summary>
  public void SetSeed(int seed){
    this.seed = seed;
    rngRange = new System.Random(seed);
  }

  /// <summary>
  /// Returns the seed stored localy.
  /// <summary>
  public int GetSeed(){
    return seed;
  }

  /// <summary>
  /// Resets the generator with the seed corresponding to the specified coordinates.
  /// Each coordiante has a unique seed.
  /// <summary>
  public void CalculateSeed(float offX, float offY) {

    int x = (int) offX;
    int y = (int) offY;

    // Calculate how far away we are from the center of the grid
    int level = Math.Max(Math.Abs(x), Math.Abs(y));
    if (level == 0) {
      UseSeed(this.seed);
    }
    //Highest value possible on the current level
    int maxLevelValue = (int) Math.Sqrt((level*2)+1)-1;
    if (x == level && y != -level) {
      UseSeed(this.seed + maxLevelValue - level*6 - level - y);
    }
    if (x == -level && y != level) {
      UseSeed(this.seed + maxLevelValue - level*2 - level + y);
    }
    if (y == level && x != level) {
      UseSeed(this.seed + maxLevelValue - level*4 - level + x);
    }
    // if (y == -level && x != -level) // Last Case Scenario
    UseSeed(this.seed + maxLevelValue - level - x);
  }
}

